miniconda image
===============

Docker_ image with a bootstrapped installation of Miniconda_ (with Python 3.6) based on CentOS 7.

The official `docker pull continuumio/miniconda3 <https://hub.docker.com/r/continuumio/miniconda3/>`_ is based on debian:8.

Docker pull command::

    docker pull registry.esss.lu.se/ics-docker/miniconda:latest


.. _Docker: https://www.docker.com
.. _Miniconda: https://conda.io/miniconda.html
