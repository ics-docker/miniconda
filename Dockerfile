FROM centos:7.8.2003

LABEL maintainer "benjamin.bertrand@esss.se"

ENV LANG=en_US.UTF-8 LC_ALL=en_US.UTF-8 LANGUAGE=en_US.UTF-8

RUN yum update -y && yum install -y \
    which \
    bzip2 \
    wget \
    https://artifactory.esss.lu.se/artifactory/rpm-ics/centos/7/x86_64/git2u-2.16.5-1.ius.centos7.x86_64.rpm \
    https://artifactory.esss.lu.se/artifactory/rpm-ics/centos/7/x86_64/git2u-core-2.16.5-1.ius.centos7.x86_64.rpm \
    https://artifactory.esss.lu.se/artifactory/rpm-ics/centos/7/x86_64/git2u-core-doc-2.16.5-1.ius.centos7.noarch.rpm \
    https://artifactory.esss.lu.se/artifactory/rpm-ics/centos/7/x86_64/git2u-perl-Git-2.16.5-1.ius.centos7.noarch.rpm \
  && yum clean all \
  && rm -rf /var/cache/yum

# Install tini
ENV TINI_VERSION v0.19.0
RUN curl -L -o /usr/local/bin/tini https://github.com/krallin/tini/releases/download/${TINI_VERSION}/tini \
  && chmod +x /usr/local/bin/tini

# Install conda
ENV CONDA_UID 33
ENV MINICONDA_INSTALLER_VERSION 4.6.14
ENV CONDA_VERSION 4.8.4
RUN curl -L -O "https://repo.continuum.io/miniconda/Miniconda3-${MINICONDA_INSTALLER_VERSION}-Linux-x86_64.sh" \
  && /bin/bash "Miniconda3-${MINICONDA_INSTALLER_VERSION}-Linux-x86_64.sh" -f -b -p /opt/conda \
  && rm "Miniconda3-${MINICONDA_INSTALLER_VERSION}-Linux-x86_64.sh" \
  && echo "conda ${CONDA_VERSION}" >> /opt/conda/conda-meta/pinned \
  && /opt/conda/bin/conda config --system --set channel_alias https://artifactory.esss.lu.se/artifactory/api/conda \
  && /opt/conda/bin/conda config --system --add channels anaconda-main \
  && /opt/conda/bin/conda config --system --add channels conda-forge \
  && /opt/conda/bin/conda config --system --add channels ics-conda-forge \
  && /opt/conda/bin/conda config --system --add channels conda-e3 \
  && /opt/conda/bin/conda config --system --remove channels defaults \
  && /opt/conda/bin/conda config --system --set auto_update_conda false \
  && /opt/conda/bin/conda config --system --set show_channel_urls true \
  && /opt/conda/bin/conda install -y conda="${CONDA_VERSION}" \
  && /opt/conda/bin/conda config --system --set use_only_tar_bz2 true \
  && /opt/conda/bin/conda clean -a -y \
  && ln -s /opt/conda/etc/profile.d/conda.sh /etc/profile.d/conda.sh \
  && useradd -m -s /bin/bash -u "$CONDA_UID" -g users conda \
  && chown -R conda:users /opt/conda

ENV PATH /opt/conda/bin:$PATH

ENTRYPOINT [ "/usr/local/bin/tini", "--" ]
CMD ["/bin/bash"]

USER conda
